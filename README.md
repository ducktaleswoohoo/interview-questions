# interview-questions

1. Given an array of user objects (i.e. { id, isActive, organization, name })
    * Sort users by name
    * Return all active users
    * Return the unique organizations, there should be no duplicates    

2. Given an object, return how many times the value "10" appears.

3. Complete Pagination Factory