/*
Utilizes Factory Function Pattern
Describe when this pattern may be used?
*/

function Paginate(lengthIn, limitIn) {
  let p = {};

  /* 
    variable definitions
    _length: size of the dataset
    _limit: number of elements per 'page'
    _pageCount: total number of 'pages'
    _curPage: current 'page' to display elements
  */

  // initialize variables
  let _length;
      _limit;
      _pageCount;
      _curPage;

  // init function
  (function() {
    // assign values to the vars
    
    _setPageCount();
  })();

  // private functions
  function _setPageCount() {
    // set page count here
  };

  // getters (add more if necessary)
  p.getLength = () => _length;
  p.getTotalPages = () => _pageCount;
  p.getCurrentIndex = () => _curPage * _limit;
  p.getLowerIndex = () => {};
  p.getUpperIndex = () => {};

  // setters
  p.setLength = (input) => {
    _length = input.length || input;
    _setPageCount();

    // what is the reason behind resetting the current page?
    p.resetCurrentPage();
  };

  p.setLimit = (input) => {

  };

  p.setPage = (direction) => {
    if (p.canMove(direction)) {
      _curPage += direction;
    }
  };

  // utility functions
  
  // canMove should return a boolean indicating whether or not we can reach the page in the given direction
  p.canMove = (direction) => {
    // direction is an integer that can be positive or negative
  };

  p.resetCurrentPage = () => {};

  // is there a way we can return this object so that it cannot be edited on the global scope?
  return p;
};
