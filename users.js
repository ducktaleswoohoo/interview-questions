let users = [
  {
      "_id" : "5660b7ff6d591ad003285097",
      "organization" : "5615548ec5ecb7841c40ed97",
      "firstName" : "Jenifer",
      "isActive" : false
  },
  {
      "_id" : "5668a576b043cfe700276f9d",
      "isActive" : false,
      "organization" : "5615548ec5ecb7841c40ed9e",
      "firstName" : "JONATHAN"
  },
  {
      "_id" : "5668a618b043cfe700276f9f",
      "firstName" : "JOSE",
      "isActive" : false,
      "organization" : "5615548ec5ecb7841c40ed9e"
  },
  {
      "_id" : "5668a6c1b043cfe700276fa1",
      "firstName" : "ERIKA",
      "organization" : "5615548ec5ecb7841c40ed9e",
      "isActive" : false
  },
  {
      "_id" : "563252af29f348eb00514087",
      "firstName" : "Luis",
      "organization" : "5615548ec5ecb7841c40ed97",
      "isActive" : true
  },
  {
      "_id" : "563252f529f348eb0051408b",
      "organization" : "5615548dc5ecb7841c40ed92",
      "firstName" : "Juan F",
      "isActive" : true
  },
  {
      "_id" : "564d5e103351f7e70074521d",
      "isActive" : true,
      "organization" : "5ae75365868f1a21ee4d0ad1",
      "firstName" : "Justin"
  },
  {
      "_id" : "5660b7766d591ad00328507c",
      "organization" : "5615548dc5ecb7841c40ed92",
      "isActive" : true,
      "firstName" : "Daniel"
  },
  {
      "_id" : "5665dbeae12724e900e6c69e",
      "isActive" : true,
      "firstName" : "James R.",
      "organization" : "5615548dc5ecb7841c40ed92"
  },
  {
      "_id" : "56689f13b043cfe700276f8d",
      "firstName" : "Victor",
      "isActive" : true,
      "organization" : "5615548ec5ecb7841c40ed9e"
  },
  {
      "_id" : "56689f43b043cfe700276f8e",
      "firstName" : "Lorena",
      "organization" : "5615548ec5ecb7841c40ed9e",
      "isActive" : true
  }
];

/*
  - Sort users by name
  - Return all active users
  - Return the unique organizations, there should be no duplicates  
*/